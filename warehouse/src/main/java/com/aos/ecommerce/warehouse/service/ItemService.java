package com.aos.ecommerce.warehouse.service;

import com.aos.ecommerce.warehouse.persistence.model.Item;

import java.util.List;

public interface ItemService {

  Item getItem(long id);

  List<Item> getAllItems();

  List<Item> getItemsList(List<Long> itemIds);

  Item updateItem(long itemId, Item item);

}
