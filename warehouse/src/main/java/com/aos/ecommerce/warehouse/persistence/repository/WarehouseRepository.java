package com.aos.ecommerce.warehouse.persistence.repository;

import com.aos.ecommerce.warehouse.persistence.model.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {
}
