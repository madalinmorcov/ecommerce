package com.aos.ecommerce.billing.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "warehouse")
@Data
public class Warehouse implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id;

  private String name;
  private String location;

  @OneToMany(
      mappedBy = "warehouse",
      cascade = CascadeType.ALL,
      orphanRemoval = true)
  @JsonIgnore
  private List<Item> items = new ArrayList<>();

  public void addItem(Item item) {
    items.add(item);
    item.setWarehouse(this);
  }

  public void deleteItem(Item item) {
    items.remove(item);
    item.setWarehouse(null);
  }

  public void deleteAllItems(){
    for(Item item : items) {
      item.setWarehouse(null);
    }
    items.clear();
  }

}
