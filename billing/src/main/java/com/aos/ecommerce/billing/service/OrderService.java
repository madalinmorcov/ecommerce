package com.aos.ecommerce.billing.service;

import com.aos.ecommerce.billing.persistence.model.Order;

public interface OrderService {

  Order createOrder(long userId);

  void completeOrder(long orderId);
}
