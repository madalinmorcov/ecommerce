package com.aos.ecommerce.billing.persistence.repository;

import com.aos.ecommerce.billing.persistence.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
