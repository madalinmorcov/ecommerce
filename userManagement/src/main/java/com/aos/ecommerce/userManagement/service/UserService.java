package com.aos.ecommerce.userManagement.service;

import com.aos.ecommerce.userManagement.persistence.model.User;

import java.util.List;

public interface UserService {
  User findById(int id);

  User findByEmail(String email);

  void saveUser(User user);

  void updateUser(User user);

  void deleteUserById(int id);

  List<User> findAllUsers();

  void deleteAllUsers();

  boolean userExists(User user);
}
