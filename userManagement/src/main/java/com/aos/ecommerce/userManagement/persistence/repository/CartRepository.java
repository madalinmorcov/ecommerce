package com.aos.ecommerce.userManagement.persistence.repository;

import com.aos.ecommerce.userManagement.persistence.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long> {
}
